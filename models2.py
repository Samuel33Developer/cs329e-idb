from flask import Flask
#from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:samuel3@localhost:5432/bookdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)



  
class Book(db.Model):
    __tablename__ = 'Book'
    #pid = db.Column(db.Integer, primary_key = True)
    Book_id = db.Column(db.Integer, primary_key = True)

    Publisher_wikipedia_url = db.Column(db.Text)
    Publisher_name = db.Column(db.String(50), nullable=False)
    Publisher_description = db.Column(db.Text)
    Publisher_owner = db.Column(db.Text)
    Publisher_image_url = db.Column(db.Text)
    Publisher_website = db.Column(db.Text)
    

#class Author(db.Model):
    #aid = db.Column(db.Integer, primary_key = True)
    Author_born = db.Column(db.Text)
    Author_name = db.Column(db.String(50), nullable=False)
    Author_education = db.Column(db.Text)
    Author_nationality = db.Column(db.Text)
    Author_description = db.Column(db.Text)
    Author_alma_mater = db.Column(db.Text)
    Author_wikipedia_url = db.Column(db.Text)
    Author_image_url = db.Column(db.Text)


#class Book(db.Model):
    #__tablename__ = 'Book'
    Book_google_id = db.Column(db.Text)
    Book_title = db.Column(db.Text, nullable=False)
    Book_isbn = db.Column(db.Text)
    Book_publication_date = db.Column(db.Text)
    Book_image_url = db.Column(db.Text)
    Book_description = db.Column(db.Text)


    def __repr__(self):
        return '<Book %r>' % self.title


db.create_all()
