def Authors():
	entries = [
	{
		'id' :'1',
		'name' : 'J.K. Rowling',
		"born": "1965-07-31",
		"nationality": "British",
		"education": "French and Classics",
		"alma_mater": "University of Exeter",
		"description": "Joanne 'Jo' Rowling, OBE, FRSL, pen names J. K. Rowling and Robert Galbraith, is a British novelist, screenwriter and film producer best known as the author of the Harry Potter fantasy series. ",
		"wiki_url": "https://en.wikipedia.org/wiki/J._K._Rowling",
		"image_url": "http://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/J._K._Rowling_2010.jpg/220px-J._K._Rowling_2010.jpg"
	},

	{
		'id' :'2',
		'name' : "Brandon Sanderson",
		"born": "1975-12-19",
		"nationality": "American",
		"education": "Bio Chemistry",
		"alma_mater": "Brigham Young University",
		"description":"Brandon Sanderson is an American fantasy and science fiction writer. He is best known for his Mistborn series and his work in finishing Robert Jordan's epic fantasy series The Wheel of Time. ",
                "alma_mater": "Brigham Young University (B.A., M.A.)",
		"wiki_url": "https://en.wikipedia.org/wiki/Brandon_Sanderson",
		"image_url": "http://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Brandon_Sanderson_sign.jpg/250px-Brandon_Sanderson_sign.jpg"
	},

	{
		'id' :'3',
		'name' : "Bob Woodward",
		"born": "1943-03-26",
		"education": "History and English literature",
		"nationality": "American",
		"alma_mater": "Yale University",
		"description":"Robert Upshur 'Bob' Woodward is an American investigative journalist and non-fiction author. He has worked for The Washington Post since 1971 as a reporter and is now an associate editor of the Post.",
		"wiki_url": "https://en.wikipedia.org/wiki/Bob_Woodward",
		"image_url":"http://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Bob_Woodward.jpg/220px-Bob_Woodward.jpg"
	}

	]

	return entries