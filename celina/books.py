def Books():
	entries = [
	{
		'id' :'1',
		'google_id': "wrOQLV6xB-wC",
		'name' : "Harry Potter and the Sorcerer's Stone",
		'isbn': "9781781100486",
		'pub_date': "1997-12-08",
		"image_url": "https://books.google.com/books/content/images/frontcover/wrOQLV6xB-wC?fife=w500",
        "description": "Turning the envelope over, his hand trembling, Harry saw a purple wax seal bearing a coat of arms; a lion, an eagle, a badger and a snake surrounding a large letter 'H'. Harry Potter has never even heard of Hogwarts when the letters start dropping on the doormat at number four, Privet Drive. Addressed in green ink on yellowish parchment with a purple seal, they are swiftly confiscated by his grisly aunt and uncle. Then, on Harry's eleventh birthday, a great beetle-eyed giant of a man called Rubeus Hagrid bursts in with some astonishing news: Harry Potter is a wizard, and he has a place at Hogwarts School of Witchcraft and Wizardry. An incredible adventure is about to begin!",
		'publishers': 'Pottermore',
		'authors': 'J.K. Rowling'
	},

	{
		'id' :'2',
		"google_id": "5iTebBW-w7QC",
		'name' : "Harry Potter and the Chamber of Secrets",
		'isbn': "9781781100509",
		'pub_date': "1998-12-08",
		"image_url": "https://books.google.com/books/content/images/frontcover/5iTebBW-w7QC?fife=w500",
        "description": "'There is a plot, Harry Potter. A plot to make most terrible things happen at Hogwarts School of Witchcraft and Wizardry this year.' Harry Potter's summer has included the worst birthday ever, doomy warnings from a house-elf called Dobby, and rescue from the Dursleys by his friend Ron Weasley in a magical flying car! Back at Hogwarts School of Witchcraft and Wizardry for his second year, Harry hears strange whispers echo through empty corridors - and then the attacks start. Students are found as though turned to stone... Dobby's sinister predictions seem to be coming true.",
		'publishers': 'Pottermore',
		'authors': 'J.K. Rowling'
	},

	{
		'id' :'3',
		"google_id": "Sm5AKLXKxHgC",
		'name' : "Harry Potter and the Prisoner of Azkaban",
		'isbn': "9781781100516",
		'pub_date': "1999-07-08",
		"image_url": "https://books.google.com/books/content/images/frontcover/Sm5AKLXKxHgC?fife=w500",
        "description": "'Welcome to the Knight Bus, emergency transport for the stranded witch or wizard. Just stick out your wand hand, step on board and we can take you anywhere you want to go.' When the Knight Bus crashes through the darkness and screeches to a halt in front of him, it's the start of another far from ordinary year at Hogwarts for Harry Potter. Sirius Black, escaped mass-murderer and follower of Lord Voldemort, is on the run - and they say he is coming after Harry. In his first ever Divination class, Professor Trelawney sees an omen of death in Harry's tea leaves ... But perhaps most terrifying of all are the Dementors patrolling the school grounds, with their soul-sucking kiss ...",
		'publishers': 'Pottermore',
		'authors': 'J.K. Rowling'
	}

	]

	return entries

def getBook(num):
	dict_books = Books()
	for book in dict_books:
		if "id" == num:
			return book