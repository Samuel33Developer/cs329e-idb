# beginning of create_db.py
import json
from models import app, db, Book

def load_json(filename):
  with open(filename) as file:
    jsn = json.load(file)
    file.close()
  return jsn

  
def create_books():
  books = load_json('data.json')
  count = 1
  for oneBook in books:

  	#populate data table information for Books

    book_google_id = oneBook.get('google_id')
    book_title = oneBook.get('title')
    book_isbn = oneBook.get('isbn')
    book_publication_date = oneBook.get('publication_date')
    book_image_url = oneBook.get('image_url')
    book_description = oneBook.get('description')
    publisher_list = oneBook.get('publishers')
    publisher_dict = publisher_list[0]

    #populate data table information for publishers
    publisher_wikipedia_url = publisher_dict.get('wikipedia_url')
    publisher_name = publisher_dict.get('name')
    publisher_owner = publisher_dict.get('owner')
    publisher_description = publisher_dict.get('description')
    publisher_image_url = publisher_dict.get('image_url')
    publisher_website = publisher_dict.get('website')
    author_list = oneBook.get('authors')
    author_dict = author_list[0]

    #populate data table information for authors
    author_birthdate = author_dict.get('born')
    author_name = author_dict.get('name')
    author_education = author_dict.get('education')
    author_nationaility = author_dict.get('nationality')
    author_description = author_dict.get('description')
    author_alma_mater = author_dict.get('alma_mater')
    author_wikipedia_url = author_dict.get('wikipedia_url')
    author_image_url = author_dict.get('image_url')




    newBook = Book(Book_id = count, Book_google_id=book_google_id , Book_title = book_title, Book_publication_date=book_publication_date,
        Book_isbn=book_isbn, Book_image_url = book_image_url, Book_description=book_description, Publisher_wikipedia_url=publisher_wikipedia_url,
        Publisher_name=publisher_name, Publisher_owner=publisher_owner, Publisher_description=publisher_description, Publisher_image_url=publisher_image_url,
        Publisher_website=publisher_website, Author_born=author_birthdate, Author_name=author_name, Author_education=author_education,
        Author_nationality=author_nationaility, Author_description=author_description, Author_alma_mater=author_alma_mater, Author_wikipedia_url=author_wikipedia_url,
        Author_image_url=author_image_url)

    # After I create the book, I can then add it to my session.
    db.session.add(newBook)

    # commit the session to my DB.
    db.session.commit()
    count += 1

# r = db.session.query(Book).filter_by(Book_id = '1').one()

# if r == None:
create_books()


 # end of create_db.py

