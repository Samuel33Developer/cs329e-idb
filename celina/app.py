from flask import Flask, render_template
from books import Books
from authors import Authors
from publishers import Publishers
from flask_paginate import Pagination, get_page_args
import subprocess
app = Flask(__name__)

book_list = Books()
author_list = Authors()
pub_list = Publishers()



@app.route('/')
def indexController():
	return render_template('home.html')

#calling people template and passing in list of people
@app.route('/Books')
def bookController():
	return render_template('books.html', books = book_list)

@app.route('/Books/1/')
def bookOne():
	return render_template('bookpage.html', book=book_list[0])

@app.route('/Books/2/')
def bookTwo():
	return render_template('bookpage.html', book=book_list[1])
@app.route('/Books/3/')
def bookThree():
	return render_template('bookpage.html', book=book_list[2])


#calling countries template and passing in list of countries
@app.route('/Authors')
def authorController():
	return render_template('authors.html', authors = author_list)

@app.route('/Authors/1/')
def authorOne():
	return render_template('authorpage.html', author = author_list[0])
@app.route('/Authors/2/')
def authorTwo():
	return render_template('authorpage.html', author = author_list[1])
@app.route('/Authors/3/')
def authorThree():
	return render_template('authorpage.html', author = author_list[2])


#calling innovations template and passing in list of innovations
@app.route('/Publishers')
def publisherController():
	return render_template('publishers.html', publishers = pub_list)

@app.route('/Publishers/1/')
def publisherOne():
	return render_template('publisherpage.html', publisher = pub_list[0])
@app.route('/Publishers/2/')
def publisherTwo():
	return render_template('publisherpage.html', publisher = pub_list[1])
@app.route('/Publishers/3/')
def publisherThree():
	return render_template('publisherpage.html', publisher = pub_list[2])

@app.route('/test')
def testController():
	p = subprocess.Popen(["coverage", "run", "--branch", "test.py"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE)
    out, err = p.communicate()
    output=err+out
    output = output.decode("utf-8") #convert from byte type to string type
    
    return render_template('test.html', output = "<br/>".join(output.split("\n")))


if __name__ == '__main__':
	app.run(debug = True)


