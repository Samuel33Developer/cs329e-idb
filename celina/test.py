import os
import sys
import unittest
#from models import db, Book
from create_db import db, Book

class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        r = db.session.query(Book).filter_by(Book_id = '1').one()
        self.assertEqual(str(r.Book_id), '1')
        self.assertEqual(str(r.Book_title), "Harry Potter and the Sorcerer's Stone")
        self.assertEqual(r.Publisher_name, "Pottermore")

    def test_source_insert_2(self):
        r = db.session.query(Book).filter_by(Book_id = '2').one()
        self.assertEqual(str(r.Book_id), '2')
        self.assertEqual(str(r.Book_title), "Harry Potter and the Chamber of Secrets")
        self.assertEqual(r.Publisher_name, "Pottermore")


    def test_source_insert_3(self):
        r = db.session.query(Book).filter_by(Book_id = '3').one()
        self.assertEqual(str(r.Book_id), '3')
        self.assertEqual(str(r.Book_title), "Harry Potter and the Prisoner of Azkaban")
        self.assertEqual(r.Publisher_name, "Pottermore")



    def test_source_insert_4(self):
        s = Book(Book_id='157', Book_title = 'Mybook')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Book).filter_by(Book_id = '157').one()
        self.assertEqual(str(r.Book_id), '157')

        db.session.query(Book).filter_by(Book_id = '157').delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()
