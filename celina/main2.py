from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy 
from sqlalchemy import or_
import os
from flask_paginate import Pagination,get_page_args
# from books import Books
# from authors import Authors
# from publishers import Publishers
# from models import app, db, Book
import subprocess
import json

app = Flask(__name__)



app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:postgres@localhost:5432/project4')
#app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:postgres@localhost:5433/project4')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)


#Populates table within the app
class Book(db.Model):
    __tablename__ = 'Book'
    #pid = db.Column(db.Integer, primary_key = True)
    Book_id = db.Column(db.Integer, primary_key = True)

    Publisher_wikipedia_url = db.Column(db.Text)
    Publisher_name = db.Column(db.String(50), nullable=True)
    Publisher_description = db.Column(db.Text)
    Publisher_owner = db.Column(db.Text)
    Publisher_image_url = db.Column(db.Text)
    Publisher_website = db.Column(db.Text)
    

#class Author(db.Model):
    #aid = db.Column(db.Integer, primary_key = True)
    Author_born = db.Column(db.Text)
    Author_name = db.Column(db.String(50), nullable=True)
    Author_education = db.Column(db.Text)
    Author_nationality = db.Column(db.Text)
    Author_description = db.Column(db.Text)
    Author_alma_mater = db.Column(db.Text)
    Author_wikipedia_url = db.Column(db.Text)
    Author_image_url = db.Column(db.Text)


#class Book(db.Model):
    #__tablename__ = 'Book'
    Book_google_id = db.Column(db.Text)
    Book_title = db.Column(db.Text, nullable=True)
    Book_isbn = db.Column(db.Text)
    Book_publication_date = db.Column(db.Text)
    Book_image_url = db.Column(db.Text)
    Book_description = db.Column(db.Text)


    def __repr__(self):
        return '<Book %r>' % self.Book_title



#Populates table in database (only run once)
db.drop_all()
db.create_all()

def load_json(filename):
  with open(filename) as file:
    jsn = json.load(file)
    file.close()
  return jsn

  
def create_books():
  books = load_json('data.json')
  count = 1
  for oneBook in books:

  	#populate data table information for Books

    book_google_id = oneBook.get('google_id')
    book_title = oneBook.get('title')
    book_isbn = oneBook.get('isbn')
    book_publication_date = oneBook.get('publication_date')
    book_image_url = oneBook.get('image_url')
    book_description = oneBook.get('description')
    publisher_list = oneBook.get('publishers')
    publisher_dict = publisher_list[0]

    #populate data table information for publishers
    publisher_wikipedia_url = publisher_dict.get('wikipedia_url')
    publisher_name = publisher_dict.get('name')
    publisher_owner = publisher_dict.get('owner')
    publisher_description = publisher_dict.get('description')
    publisher_image_url = publisher_dict.get('image_url')
    publisher_website = publisher_dict.get('website')
    author_list = oneBook.get('authors')
    author_dict = author_list[0]

    #populate data table information for authors
    author_birthdate = author_dict.get('born')
    author_name = author_dict.get('name')
    author_education = author_dict.get('education')
    author_nationaility = author_dict.get('nationality')
    author_description = author_dict.get('description')
    author_alma_mater = author_dict.get('alma_mater')
    author_wikipedia_url = author_dict.get('wikipedia_url')
    author_image_url = author_dict.get('image_url')




    newBook = Book(Book_id = count, Book_google_id=book_google_id , Book_title = book_title, Book_publication_date=book_publication_date,
        Book_isbn=book_isbn, Book_image_url = book_image_url, Book_description=book_description, Publisher_wikipedia_url=publisher_wikipedia_url,
        Publisher_name=publisher_name, Publisher_owner=publisher_owner, Publisher_description=publisher_description, Publisher_image_url=publisher_image_url,
        Publisher_website=publisher_website, Author_born=author_birthdate, Author_name=author_name, Author_education=author_education,
        Author_nationality=author_nationaility, Author_description=author_description, Author_alma_mater=author_alma_mater, Author_wikipedia_url=author_wikipedia_url,
        Author_image_url=author_image_url)

    # After I create the book, I can then add it to my session.
    db.session.add(newBook)

    # commit the session to my DB.
    db.session.commit()
    count += 1

# r = db.session.query(Book).filter_by(Book_id = '1').one()

# if r == None:
create_books()


 # end of create_db.py




#moved query
books= db.session.query(Book).all()

authors = db.session.query(Book.Author_name, Book.Author_born, Book.Author_nationality, Book.Author_education, Book.Author_alma_mater).group_by(Book.Author_name, Book.Author_born, Book.Author_nationality, Book.Author_education, Book.Author_alma_mater).all()

pub = db.session.query(Book.Publisher_name,Book.Publisher_owner,Book.Publisher_image_url,Book.Publisher_website).group_by(Book.Publisher_name,Book.Publisher_owner,Book.Publisher_image_url,Book.Publisher_website).all()


@app.route('/About/')
def indexController():
    """Renders the about page"""
    return render_template('about.html')

@app.route('/')
def aboutController():
    """Renders the home page"""
    return render_template('home.html')

def get_books(offset=0, per_page=10):
    """splits results from books in increments of 10 for pagination"""
    return books[offset: offset + per_page]

def get_authors(offset=0, per_page=10):
    """splits results from author in increments of 10 for pagination"""
    return authors[offset: offset + per_page]

def get_publishers(offset=0, per_page=10):
    """splits results from publisher in increments of 10 for pagination"""
    return pub[offset: offset + per_page]



@app.route('/Books/')
def bookController():
    """Renders the book page with a list of all books in our database"""
    page, per_page, offset = get_page_args(page_parameter='page', per_page_parameter='per_page')
    
    total = len(books)
    pagination_books = get_books(offset=offset, per_page=per_page)
    pagination = Pagination(page=page, per_page=per_page, total=total,css_framework="bootstrap4")
    return render_template('books.html', books=pagination_books, page=page, per_page=per_page, pagination=pagination)

@app.route('/Books/<int:Book_id>/', methods=['GET'])
def bookOne(Book_id):
    """Renders a page for a single book chosen by the user"""
    book = Book.query.get(Book_id)
    publisher_books = db.session.query(Book.Publisher_name).filter_by(Publisher_name=book.Publisher_name).group_by(Book.Publisher_name).all()
    return render_template('bookpage.html', book=book, Publisher_books=publisher_books)


@app.route('/Search/', methods=['GET', 'POST'])
def searchController():
  """Renders the search page with a list of all matched results relevant to our 3 crisis: Book, Publisher, Author"""
  if (request.method=='POST'):
    
    if (request.form["search"].strip() == ""):
      return render_template('search.html', books=[])
    else:
      query=request.form["search"]
      query = "%"+query+"%"
      book=db.session.query(Book).filter(or_(Book.Book_title.ilike(query), Book.Publisher_name.ilike(query), Book.Author_name.ilike(query)))
      return render_template('search.html', books=book)
  return render_template('search.html', books=[])

"""
@app.route('/SearchResults/<string:Query>/', methods=['GET'])
def SearchResults(Query):
	print(Query)
	return render_template('searchresults.html', query=Query)
"""
@app.route('/Authors')
def authorController():
    """Renders the authors page with a list of all authors"""


    page, per_page, offset = get_page_args(page_parameter='page', per_page_parameter='per_page')
    total = len(authors)
    pagination_authors = get_authors(offset=offset, per_page=per_page)
    pagination = Pagination(page=page, per_page=per_page, total=total,css_framework="bootstrap4")
    return render_template('authors.html', authors=pagination_authors, page=page, per_page=per_page, pagination=pagination)



@app.route('/Authors/<string:Author_name>/', methods=['GET'])
def authorOne(Author_name):
    """Renders a page for a single author chosen by the user"""
    author = Book.query.filter_by(Author_name=Author_name).first()
    author_books = db.session.query(Book).filter_by(Author_name=Author_name).all()
    publisher_books = db.session.query(Book.Publisher_name).filter_by(Author_name=Author_name).group_by(Book.Publisher_name).all()
    #end of sam's code
    return render_template('authorpage.html', book=author, Author_Books=author_books, Publisher_Books=publisher_books)

@app.route('/Publishers')
def publisherController():
    """Renders the publishers page with a list of all publishers"""
    page, per_page, offset = get_page_args(page_parameter='page', per_page_parameter='per_page')
    total = len(pub)
    pagination_publishers = get_publishers(offset=offset, per_page=per_page)
    pagination = Pagination(page=page, per_page=per_page, total=total,css_framework="bootstrap4")
    return render_template('publishers.html', publishers=pagination_publishers, page=page, per_page=per_page, pagination=pagination)

@app.route('/Publishers/<string:Publisher_name>/', methods=['GET'])
def publisherOne(Publisher_name):
    """Renders a page for a single publisher chosen by the user"""
    publisher = Book.query.filter_by(Publisher_name=Publisher_name).first()
    authors = Book.query.filter_by(Publisher_name=Publisher_name).all()
    authors1 = Book.query.filter_by(Publisher_name=Publisher_name).distinct(Book.Author_name)
    #author_books = db.session.query(Book).filter_by(Author_name=Author_name).all()
    return render_template('publisherpage.html', publisher=publisher, Author_Books=authors, Author_Books2=authors1)

@app.route('/test/')
def testController():
    """Renders the test page that contains results from unit tests"""
    return render_template('test.html')


if __name__ == '__main__':
	app.run(debug = True)

