from flask import Flask
#from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:postgres@localhost:5432/project4')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)



  
class Book(db.Model):
    __tablename__ = 'Book'
    #pid = db.Column(db.Integer, primary_key = True)
    Book_id = db.Column(db.Integer, primary_key = True)

    Publisher_wikipedia_url = db.Column(db.Text)
    Publisher_name = db.Column(db.String(50), nullable=True)
    Publisher_description = db.Column(db.Text)
    Publisher_owner = db.Column(db.Text)
    Publisher_image_url = db.Column(db.Text)
    Publisher_website = db.Column(db.Text)
    

#class Author(db.Model):
    #aid = db.Column(db.Integer, primary_key = True)
    Author_born = db.Column(db.Text)
    Author_name = db.Column(db.String(50), nullable=True)
    Author_education = db.Column(db.Text)
    Author_nationality = db.Column(db.Text)
    Author_description = db.Column(db.Text)
    Author_alma_mater = db.Column(db.Text)
    Author_wikipedia_url = db.Column(db.Text)
    Author_image_url = db.Column(db.Text)


#class Book(db.Model):
    #__tablename__ = 'Book'
    Book_google_id = db.Column(db.Text)
    Book_title = db.Column(db.Text, nullable=True)
    Book_isbn = db.Column(db.Text)
    Book_publication_date = db.Column(db.Text)
    Book_image_url = db.Column(db.Text)
    Book_description = db.Column(db.Text)


    def __repr__(self):
        return '<Book %r>' % self.Book_title

#why should we do this? Would it be better to run create_db once?
#where is create_all defined? 
#these two commands were creating problems with the connection between the database and the program.
#couldn't print out books
#only works if we run models.py and create_db.py with drop all and create all, then comment out to run main2.py
# db.drop_all()
# db.create_all()